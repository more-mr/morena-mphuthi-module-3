import 'package:flutter/material.dart';
import './login.dart';
import './register.dart';
import './dashboard.dart';
import './editpro.dart';
import './iteml.dart';
import './noticl.dart';

void main() => runApp(AppStart());

class AppStart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     initialRoute: "/",
      routes: {
        "/": (context) => Login(), 
        "/Register" : (context) => Register(),
        "/Dashboard": (context) => Dashboard(),
        "/Dashboard/EditProfile": (context) => EditProfile(),
        "/Dashboard/ItemList": (context) => ItemList(),
        "/Dashboard/NoticList": (context) => NoticList(),
       }
      );//MaterialApp
  }
}

// Create a Flutter app that has the following screens,
// navigating to each other. Your app must have up to 6 screens
// (login, dashboard, 2 feature screens, and user profile edit):

// Login and registration screens (not linked to a database) with relevant input fields.
// Dashboard (the screen after login) with buttons to feature screens of your app.
// The last screen must be of a user profile edit.
// Dashboard Floating button on the home screen, linking to another screen.
// Each screen must be labeled appropriately on the app bar.
